# knowledgejunkie-apt-source

## Description

A [Debian GNU/Linux][debian] metapackage to ease installation of apt sources
and release/package pins.

This package installs apt sources for:

- Debian testing/sid
- local file repository
- 3rd party deb-multimedia repository

This package also installs package-specific pins for commonly used packaging
software when building package for the following teams:

- Debian Go
- Debian Perl
- Debian Python


## Building the metapackage

Build the binary package using git-buildpackage:

    $ gbp buildpackage

This will generate the binary .deb in gbp's configured output directory.


## Installing the metapackage

We can install the package using gdebi, which will automatically install
any missing dependencies

    # gdebi knowledgejunkie-apt-source_<version>_all.deb


## License

Copyright: 2020, Nick Morrott <nickm@debian.org>
License: GPL-2+


## Thanks

I hope you find this metapackage useful. Feel free to follow me on [GitHub][github] and [Twitter][twitter].

[debian]: http://www.debian.org/
[github]: https://github.com/knowledgejunkie
[twitter]: http://twitter.com/nickmorrott
